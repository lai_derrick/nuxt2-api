<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\UserController;
use App\Http\Controllers\Api\V1\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// 用戶
Route::group(['prefix' => 'v1/user', 'as' => 'api.'],function ($router) {
    $router->post('login',     [UserController::class, 'login']);
    $router->post('register',  [UserController::class, 'register']);

    $router->middleware('auth:api')->group(function ($router) {
        $router->get('information',  [UserController::class, 'information']);
        $router->get('menu',         [UserController::class, 'menu']);
        $router->post('logout',      [UserController::class, 'logout']);
        $router->post('refresh',     [UserController::class, 'refresh']);
        $router->post('upload',      [UserController::class, 'upload']);
        $router->put('/',            [UserController::class, 'profile']);
    });
});

Route::group(['prefix' => 'v1'],function ($router) {
    Route::apiResource('product', ProductController::class);
});
