<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\productSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            categorySeeder::class,
            productSeeder::class,
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
