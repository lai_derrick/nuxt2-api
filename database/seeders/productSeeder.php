<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class productSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create = [
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img47.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img41.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img42.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img43.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img46.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img48.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img49.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img49.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img43.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img46.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img43.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img46.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img48.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img49.png',
            ],
            [
                'name' => '不锈钢汤勺',
                'category_id' =>1,
                'price' => '300',
                'image' => env('APP_URL').'/images/img49.png',
            ],
        ];
        foreach ($create as  $value) {
            Product::create($value);
        }
    }
}
