<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class categorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $create = [
            [
                'name' => '不鏽鋼',
            ],
            [
                'name' => '原料水泥',
            ],
            [
                'name' => '塑料',
            ],
            [
                'name' => '木質',
            ]
        ];
        foreach ($create as  $value) {
            Category::create($value);
        }
    }
}
