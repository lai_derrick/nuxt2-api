<?php

namespace App\Models;

use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;

     /**
     * 關聯 Categorie Table
     */
    public function categorie()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
