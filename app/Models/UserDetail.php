<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\UserDetail
 */
class UserDetail extends Model
{
    protected $guarded  = [];

    protected $appends  = ['avatar'];

    /**
     * 解析頭像
     *
     * @param  string  $value
     * @return string
     */
    public function getAvatarAttribute()
    {
        return [
            'path' => $this->avatar_path,
            'url'  => Str::is('http*', $this->avatar_path)?$this->avatar_path:Storage::disk('public')->url($this->avatar_path)
        ];
    }
}
